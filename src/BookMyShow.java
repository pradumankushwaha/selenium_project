import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class BookMyShow {
    public static void main(String[] args) throws Exception{
            System.setProperty("webdriver.chrome.driver", "/home/pradumankushwaha/chromedriver/chromedriver");
            WebDriver driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            driver.get("https://in.bookmyshow.com/");
            driver.manage().window().maximize();
            WebElement city = driver.findElement(By.xpath("//*[@id=\"modal-root\"]/div/div/div/div[1]/div/div/input"));
            city.sendKeys("Mumbai");
            city.sendKeys(Keys.ENTER);
//        String parentHandel = driver.getWindowHandle();
//        System.out.println(parentHandel);
//        Set<String> handles=driver.getWindowHandles();
//        for(String handle:handles){
//          if(!handle.equals(parentHandel)){
//              driver.switchTo().window(handle);
//              driver.findElement(By.xpath("//*[@id=\"super-container\"]/div[2]/div[3]/div[1]/div[2]/div/div/div/div[2]/div/div[1]/div[3]/a/div/div[2]/div/img")).click();
//
//          }
//          else {
//              driver.findElement(By.xpath("//*[@id=\"super-container\"]/div[2]/div[3]/div[1]/div[2]/div/div/div/div[2]/div/div[1]/div[3]/a/div/div[2]/div/img")).click();
//          }
//        }
            driver.findElement(By.xpath("//*[@id=\"super-container\"]/div[2]/div[3]/div[1]/div[2]/div/div/div/div[2]/div/div[1]/div[3]/a/div/div[2]/div/img")).click();
            driver.findElement(By.xpath("//*[@id=\"page-cta-container\"]/button/div/span")).click();
            driver.findElement(By.xpath("//*[@id=\"venuelist\"]/li[2]/div[2]/div[2]/div[3]/a/div/div")).click();
            driver.findElement(By.id("btnPopupAccept")).click();
            driver.findElement(By.xpath("//*[@id=\"pop_5\"]")).click();
            driver.findElement(By.id("proceed-Qty")).click();
            driver.findElement(By.xpath("//*[@id=\"B_4_026\"]/a")).click();
            driver.findElement(By.id("btnSTotal_reserve")).click();

    }
}
