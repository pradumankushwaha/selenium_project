import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.Set;
import java.util.logging.Handler;

public class Flipkart {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","/home/pradumankushwaha/chromedriver/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://www.flipkart.com/");
        driver.manage().window().maximize();
       //driver.findElement(By.className("_2KpZ6l _2doB4z")).click();
        driver.findElement(By.xpath("/html/body/div[2]/div/div/button")).click();
        driver.findElement(By.className("_3704LK")).sendKeys("onePlus");
        driver.findElement(By.className("L0Z3Pu")).click();
       // driver.findElement(By.className("_4rR01T")).click();
        driver.findElement(By.xpath("//div[contains(text(),'OnePlus Y1 80 cm (32 inch) HD Ready LED Smart Andr')]")).click();
         //Handler=driver.getWindowHandle();
        String parentHandle = driver.getWindowHandle();
        System.out.println(parentHandle);
        Set<String> handles = driver.getWindowHandles();
        for(String handle:handles){
            if(!handle.equals(parentHandle)){
                driver.switchTo().window(handle);
                driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[3]/div[1]/div[1]/div[2]/div/ul/li[1]/button")).click();
                 driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[2]/div/div/div[1]/div/div[3]/div/form/button")).click();
                 driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[2]/div/div[1]/div[1]/div/div/div/div/div[1]/div/form/div[1]/input")).sendKeys("8009711198");
            }
            Thread.sleep(5000);
            driver.close();
        }
        Thread.sleep(5000);
       driver.close();
    }
}


